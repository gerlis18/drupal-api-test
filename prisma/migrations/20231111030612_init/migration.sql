-- CreateTable
CREATE TABLE "Inmueble" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "codigoInmueble" TEXT NOT NULL,
    "tipo" TEXT NOT NULL,
    "estrato" TEXT NOT NULL,
    "barrio" TEXT NOT NULL,
    "asegurado" TEXT NOT NULL,
    "valorAdquisicion" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Inmueble_codigoInmueble_key" ON "Inmueble"("codigoInmueble");
