-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Inmueble" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nombre" TEXT NOT NULL,
    "clasificacion" TEXT NOT NULL,
    "direccion" TEXT NOT NULL,
    "barrio" TEXT NOT NULL,
    "clase" TEXT NOT NULL,
    "tipo" TEXT NOT NULL,
    "tipo_documento" TEXT NOT NULL,
    "numero_documento" TEXT NOT NULL,
    "fecha_documento" TEXT NOT NULL,
    "oficina_expedicion_documento" TEXT,
    "departamento_documento" TEXT NOT NULL,
    "ciudad_documento" TEXT NOT NULL,
    "npn" TEXT NOT NULL
);
INSERT INTO "new_Inmueble" ("barrio", "ciudad_documento", "clase", "clasificacion", "departamento_documento", "direccion", "fecha_documento", "id", "nombre", "npn", "numero_documento", "oficina_expedicion_documento", "tipo", "tipo_documento") SELECT "barrio", "ciudad_documento", "clase", "clasificacion", "departamento_documento", "direccion", "fecha_documento", "id", "nombre", "npn", "numero_documento", "oficina_expedicion_documento", "tipo", "tipo_documento" FROM "Inmueble";
DROP TABLE "Inmueble";
ALTER TABLE "new_Inmueble" RENAME TO "Inmueble";
CREATE UNIQUE INDEX "Inmueble_nombre_key" ON "Inmueble"("nombre");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
