/*
  Warnings:

  - You are about to drop the column `asegurado` on the `Inmueble` table. All the data in the column will be lost.
  - You are about to drop the column `codigoInmueble` on the `Inmueble` table. All the data in the column will be lost.
  - You are about to drop the column `estrato` on the `Inmueble` table. All the data in the column will be lost.
  - You are about to drop the column `valorAdquisicion` on the `Inmueble` table. All the data in the column will be lost.
  - Added the required column `ciudad_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `clase` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `clasificacion` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `departamento_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `direccion` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `fecha_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `nombre` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `npn` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `numero_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `oficina_expedicion_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.
  - Added the required column `tipo_documento` to the `Inmueble` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Inmueble" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nombre" TEXT NOT NULL,
    "clasificacion" TEXT NOT NULL,
    "direccion" TEXT NOT NULL,
    "barrio" TEXT NOT NULL,
    "clase" TEXT NOT NULL,
    "tipo" TEXT NOT NULL,
    "tipo_documento" TEXT NOT NULL,
    "numero_documento" TEXT NOT NULL,
    "fecha_documento" TEXT NOT NULL,
    "oficina_expedicion_documento" TEXT NOT NULL,
    "departamento_documento" TEXT NOT NULL,
    "ciudad_documento" TEXT NOT NULL,
    "npn" TEXT NOT NULL
);
INSERT INTO "new_Inmueble" ("barrio", "id", "tipo") SELECT "barrio", "id", "tipo" FROM "Inmueble";
DROP TABLE "Inmueble";
ALTER TABLE "new_Inmueble" RENAME TO "Inmueble";
CREATE UNIQUE INDEX "Inmueble_nombre_key" ON "Inmueble"("nombre");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
