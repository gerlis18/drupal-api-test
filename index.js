const express = require('express');
const cors = require('cors');
const { PrismaClient } = require('@prisma/client');
const bodyParser = require('body-parser');
const { z } = require('zod');

const prisma = new PrismaClient()
const app = express();

app.use(bodyParser.json());
app.use(cors());

const inmuebleSchema = z.object({
  nombre: z.string({
    required_error: 'nombre es requerido'
  }),
  clasificacion: z.string({
    required_error: 'clasificacion es requerido'
  }),
  direccion: z.string({
    required_error: 'direccion es requerido'
  }),
  barrio: z.string({
    required_error: 'barrio es requerido'
  }),
  clase: z.string({
    required_error: 'clase es requerido'
  }),
  tipo: z.string({
    required_error: 'tipo es requerido'
  }),
  tipo_documento: z.string({
    required_error: 'tipo_documento es requerido'
  }),
  numero_documento: z.string({
    required_error: 'numero_documento es requerido'
  }),
  fecha_documento: z.string({
    required_error: 'fecha_documento es requerido'
  }),
  departamento_documento: z.string({
    required_error: 'departamento_documento es requerido'
  }),
  ciudad_documento: z.string({
    required_error: 'ciudad_documento es requerido'
  }),
  npn: z.string({
    required_error: 'npn es requerido'
  }),
})

app.post('/api/inmuebles', async (req, res) => {
  try {
    const body = inmuebleSchema.parse(req.body);
    const inmueble = await prisma.inmueble.create({
      data: body
    });
    res.status(201).json(inmueble);
  } catch (error) {
    console.log('error', error)
    res.status(400).json({
      msg: 'error creando inmueble',
      error
    })
  }
});

app.put('/api/inmuebles/:id', async (req, res) => {
  try {
    const inmueble = await prisma.inmueble.update({
      where: { id: req.params.id },
      data: req.body
    });
    res.status(200).json(inmueble);
  } catch (error) {
    res.status(400).json({
      msg: 'fallo al actualizar inmueble',
      error
    })
  }
});

app.get('/api/inmuebles', async (req, res) => {
  const list = await prisma.inmueble.findMany();
  res.status(200).json(list);
});

app.listen(process.env.PORT || 3000, () => {
  console.log('loaded');
});